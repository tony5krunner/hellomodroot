package hellomodroot

import "fmt"

func SayHello() {
	fmt.Println("Hello from hellomod module (v2.0.3)")
}
